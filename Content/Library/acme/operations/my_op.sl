########################################################################################################################
#!!
#! @description: Generated operation description
#!
#! @input input_1: Generated description
#! @input input_2: Generated description
#!
#! @output output: Generated description
#!
#! @result SUCCESS: Operation completed successfully.
#! @result FAILURE: Failure occured during execution.
#!!#
########################################################################################################################

namespace: content.library.acme.operations

operation:
    name: my_op

    inputs:
      - input_1
      - input_2

    python_action:
        script: |
          print input_1
          output = "123"

    outputs:
      - output: ${str(output)}

    results:
      - SUCCESS: ${input_2 == '123'}
      - FAILURE