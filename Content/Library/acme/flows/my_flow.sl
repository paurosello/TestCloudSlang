########################################################################################################################
#!!
#! @description: Generated flow description
#!
#! @input input_1: Generated description
#! @input input_2: Generated description
#!
#! @output output: Generated description
#!
#! @result SUCCESS: Flow completed successfully.
#! @result FAILURE: Failure occurred during execution.
#!!#
########################################################################################################################

namespace: content.library.acme.flows
imports:
  ops: content.library.acme.operations

flow:
  name: my_flow

  inputs:
    - input_1
    - input_2

  workflow:
    - step1:
        parallel_loop:
          for: attempt in range(1,500)
          do:
            ops.my_op:
             - input_1: ${input_1}
             - input_2: ${input_2}
        publish:
          - output
        navigate:
          - SUCCESS: SUCCESS
          - FAILURE: FAILURE

  outputs:
    - output

  results:
    - SUCCESS
    - FAILURE